//1 Задание

function concat(a:string, b:string):string{
return `${a} ${b}`;
};

const c = concat('Hello', 'World');


//2 Задание

interface IMyHometask {
    howIDoIt: string;
    simeArray: (string | number)[];
    withData: {
        howIDoIt: string;
        simeArray: (string | number)[];
    }[];
};

const MyHometask:IMyHometask = {
    howIDoIt: "I Do It Wel",
    simeArray: ["string one", "string two", 42],
    withData: [{ howIDoIt: "I Do It Wel", simeArray: ["string one", 23] }],
};

//3 Задание

interface MyArray<T> {
    [N: number]: T;

    reduce( fn: (accumulator: number, value: T) => T, initialValue: number): T;

};

const initialValue: number = 0;

const arr: MyArray<number> = [1, 2, 3];

const num = arr.reduce((acc, val) => acc + val, initialValue)


//4 Задание

interface IHomeTask {

    data: string;

    numbericData: number;

    date: Date;

    externalData: {

        basis: number;

        value: string;

    }

}
type MyPartial<T> = {

    [N in keyof T] ?: T[N] extends object ? MyPartial<T[N]> : T[N]

}

const homeTask: MyPartial<IHomeTask> = {

    externalData: {

        value: 'win'

    }
}
