/*
Что здесь нужно изменить:
    1.Нам нужно изменит процесс нашей компиляции:
        Сейчас мы создаём общий  конфиг и вызываем compiler run  и cpmpiler watch для общего конфига
        Нам же необходимо их изменить, серверную конфигурацию мы оставим как есть, а к клиентской мы будет подключить HMR 
        и настраивать его соответственно отдельным сервером

        Для это разделим наш webpack.config на server.config и client.config: const [webpackClientConfig, webpackServerConfig] = require('../webpack.config')
*/
const nodemon = require('nodemon');

const path = require('path');


const webpack = require('webpack');


// Клиентская часть

const [webpackClientConfig, webpackServerConfig] = require('../webpack.config');

// Теперь на нужно подключить необходимые для создания dev-server -а midle wear который мы установили 
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');


const express = require('express');
//Подготовили хмр сервер
const hmrServer = express();

const clientCompiler = webpack(webpackClientConfig)


hmrServer.use(webpackDevMiddleware(clientCompiler, {
    publicPath: webpackClientConfig.output.publicPath,
    serverSideRender: true,
    noInfo: true,
    watchOptions: {
        ignore: /dist/,
    },
    writeToDisk: true,
    stats: 'errors-only',
}));

hmrServer.use(webpackHotMiddleware(clientCompiler, {
    path: '/static/__webpack_hmr',
}));

hmrServer.listen(3001, () => {
    console.log('HMR server succesful started')
});







//Серверная часть

//В компаилер передаём серверконфиг и серверную часть больше не трогаем она остаётся такая какая есть
const compiler = webpack(webpackServerConfig);

compiler.run((err) => {
    if(err) {
        console.log('Compiletion failed: ', err)
    }

    compiler.watch({}, () => {
        if(err) {
            console.log('Compiletion failed: ', err)
        }
        console.log('Compiler was succefully');
    });

    nodemon({
        script: path.resolve(__dirname, '../dist/server/server.js'),
        watch: [
            path.resolve(__dirname, '../dist/server'),
            path.resolve (__dirname, '../dist/client'),
        ]
    })
});

// чтобы запустить пишем node ./bin/dev.js