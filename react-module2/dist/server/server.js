/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/shared/header.less":
/*!********************************!*\
  !*** ./src/shared/header.less ***!
  \********************************/
/***/ ((module) => {

eval("// Exports\nmodule.exports = {\n\t\"example\": \"header__example--45ehT\"\n};\n\n\n//# sourceURL=webpack://ww/./src/shared/header.less?");

/***/ }),

/***/ "./src/server/indexTimplate.js":
/*!*************************************!*\
  !*** ./src/server/indexTimplate.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";
eval("\r\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\r\nexports.indexTimplate = void 0;\r\n// Эта функция которая возвращает html, content добавляем в div\r\n//<script src=\"/static/client.js\" type=\"application/javascript\"></script> -> этот скрипт будет загружать клиенский бандле\r\n//indexTimplate импортируем в server.js и добавляем его в \r\n// app.get('/', (req, res) => {\r\n//     res.send(\r\n//        indexTimplate(ReactDOM.renderToString(Header())),\r\n//     );\r\n// });\r\nconst indexTimplate = (content) => `\r\n    <!DOCTYPE html>\r\n    <html lang=\"en\">\r\n    <head>\r\n        <meta charset=\"UTF-8\">\r\n        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n        <title>Document</title>\r\n        <script src=\"/static/client.js\" type=\"application/javascript\"></script>  \r\n    </head>\r\n    <body>\r\n        <div id=\"react-root\">${content}</div>\r\n    </body>\r\n    </html>\r\n`;\r\nexports.indexTimplate = indexTimplate;\r\n\n\n//# sourceURL=webpack://ww/./src/server/indexTimplate.js?");

/***/ }),

/***/ "./src/server/server.js":
/*!******************************!*\
  !*** ./src/server/server.js ***!
  \******************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";
eval("\r\n// // мы можем просто написать и тогда node js поймет наш код, однако чуть позже мы будем рендерить рект компоненты и для них нужно бует использовать систему модулей из es6, поэтому нам придется оставить импорт express\r\n// const express = require('express');\r\nvar __importDefault = (this && this.__importDefault) || function (mod) {\r\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\r\n};\r\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\r\n// импорт не работает потому что мы используем es6 модуль import, настроим webpack таким образом, чтобы он работал с нашим серверным файлом и генерировал нам серверный bundle, который мы\r\n//сможем запустить при помоще node ./src/server/server.js, и генерировал нам клиентский bundle\r\nconst express_1 = __importDefault(__webpack_require__(/*! express */ \"express\"));\r\n// Импортируем реакт дом. react-dom/server это билиеотека ract dom видимо для сервера \r\nconst server_1 = __importDefault(__webpack_require__(/*! react-dom/server */ \"react-dom/server\"));\r\n// Импортируем компонент\r\nconst Header_1 = __webpack_require__(/*! ../shared/Header */ \"./src/shared/Header.jsx\");\r\nconst indexTimplate_1 = __webpack_require__(/*! ./indexTimplate */ \"./src/server/indexTimplate.js\");\r\n// иницилизируем наше приложение. в которой вызываем экспресс как функцию, теперь app это incest нашего приложения\r\nconst app = (0, express_1.default)();\r\n//Эта запись значит - мы определили, что по url \"/static\" будут доступны все файлы  которые лежат в папке './dist/client', в том числе и client.js\r\napp.use('/static', express_1.default.static('./dist/client'));\r\n// устанавливаем листенер на метод get. браузер отправляет get запросы когда мы переходим на страницу. и опредилим roat на которой она(страница) будет доступна\r\n// вторым аргументом передаём hendler который будет принимать, два аргумента, это request (req) и response (res)\r\n//каждый раз когда нам будет приходить запрос на корневой url, то есть на '/', express будет вызывать для нас callback, который будет передовать две переменные request (req) и response (res)\r\n//request (req) содержит информацию о пришедшем request-e (запросе), а response (res)(перевод отклик) это то что мы формируе\r\napp.get('/', (req, res) => {\r\n    res.send(\r\n    // Функция indexTimplate прнимает контент, этот контен возвращает ReactDOM.renderToString(Header()), а темплэйт отправлять с сервера\r\n    (0, indexTimplate_1.indexTimplate)(server_1.default.renderToString((0, Header_1.Header)())));\r\n});\r\n// теперь вызываем наш серевер, для этого мы вызываем функцию listen, и передаём в качетсве аргумента 3000-ый порт, в качестве второго аргументиа передаём callback\r\napp.listen(3000, () => {\r\n    // теперь каждый раз когда сервер будет запускаться он будет ввыводить в консль это сообщение\r\n    console.log('Server started on http://localhost:3000');\r\n});\r\n//чтобы посмотреть работает наш серевер node ./src/server/server.js (передаём путь к серверу )\r\n\n\n//# sourceURL=webpack://ww/./src/server/server.js?");

/***/ }),

/***/ "./src/shared/Header.jsx":
/*!*******************************!*\
  !*** ./src/shared/Header.jsx ***!
  \*******************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";
eval("\r\nvar __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {\r\n    if (k2 === undefined) k2 = k;\r\n    var desc = Object.getOwnPropertyDescriptor(m, k);\r\n    if (!desc || (\"get\" in desc ? !m.__esModule : desc.writable || desc.configurable)) {\r\n      desc = { enumerable: true, get: function() { return m[k]; } };\r\n    }\r\n    Object.defineProperty(o, k2, desc);\r\n}) : (function(o, m, k, k2) {\r\n    if (k2 === undefined) k2 = k;\r\n    o[k2] = m[k];\r\n}));\r\nvar __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {\r\n    Object.defineProperty(o, \"default\", { enumerable: true, value: v });\r\n}) : function(o, v) {\r\n    o[\"default\"] = v;\r\n});\r\nvar __importStar = (this && this.__importStar) || function (mod) {\r\n    if (mod && mod.__esModule) return mod;\r\n    var result = {};\r\n    if (mod != null) for (var k in mod) if (k !== \"default\" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);\r\n    __setModuleDefault(result, mod);\r\n    return result;\r\n};\r\nvar __importDefault = (this && this.__importDefault) || function (mod) {\r\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\r\n};\r\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\r\nexports.Header = void 0;\r\n// Адапптируем файл под HMR\r\nconst root_1 = __webpack_require__(/*! react-hot-loader/root */ \"react-hot-loader/root\");\r\nconst React = __importStar(__webpack_require__(/*! react */ \"react\"));\r\n// Добавляем стили \r\nconst header_less_1 = __importDefault(__webpack_require__(/*! ./header.less */ \"./src/shared/header.less\"));\r\nfunction HeaderComponent() {\r\n    return (React.createElement(\"header\", null,\r\n        React.createElement(\"h1\", { className: header_less_1.default.example }, \"Hello World\")));\r\n}\r\nexports.Header = (0, root_1.hot)(HeaderComponent);\r\n\n\n//# sourceURL=webpack://ww/./src/shared/Header.jsx?");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/***/ ((module) => {

"use strict";
module.exports = require("express");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ "react-dom/server":
/*!***********************************!*\
  !*** external "react-dom/server" ***!
  \***********************************/
/***/ ((module) => {

"use strict";
module.exports = require("react-dom/server");

/***/ }),

/***/ "react-hot-loader/root":
/*!****************************************!*\
  !*** external "react-hot-loader/root" ***!
  \****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react-hot-loader/root");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__("./src/server/server.js");
/******/ 	
/******/ })()
;