// // Это HtmlWebpackPluginупрощает создание файлов HTML для обслуживания ваших пакетов веб-пакетов. 
// // Это особенно полезно для пакетов веб-пакетов, которые включают хеш в имени файла, 
// // который изменяется при каждой компиляции.
// const HtmlWebpackPlugin = require('html-webpack-plugin');

// const path = require('path');

// const NODE_ENV = process.env.NODE_ENV;

// // Чтобы Dev сервер работал только devolopment режиме
// const IS_DEV = NODE_ENV == 'development';

// const IS_PROD = NODE_ENV == 'prodaction';

// function setupDevTool () {
//     if (IS_DEV) return 'eval';
//     if (IS_PROD) return false;
// }

// module.exports = {

//     // Эти параметры изменяют способ разрешения модулей. Webpack предоставляет разумные значения по умолчанию, но можно изменить разрешение в деталях.
//     resolve: {

//         // Попытайтесь разрешить эти расширения по порядку. Если несколько файлов имеют одно и то же имя, но разные расширения,
//         //  webpack разрешит файл с расширением, указанным первым в массиве, и пропустит остальные.
//         extensions: ['.jsx', '.js', '.json'],
//     },

//     // Значение мод принимает строку, которая будет либо деволпмент либо продакшн, для того чтобы решать это динамически и не хардкодить, прямо в конфиге, воспользуемся ENV перемеными 
//     // Это строка NODE_ENV ? NODE_ENV означет что если есть в нашей переменной NODE_ENV какая нибудь строка, если есть использовать ее, если нет то 'development'
//     // "scripts": {
//     //     "build:dev": "env NODE_ENV=development webpack --config webpack.config.js",
//     //     "build:prod": "env NODE_ENV=prodaction webpack --config webpack.config.js",
//     mode: NODE_ENV ? NODE_ENV: 'development',
    
//     //точка входа
//     entry: path.resolve(__dirname, 'src/index.jsx'),
    
//     // Указываем вебпаку куда складывать, все файлы которые он переработает
//     output: {
//         path: path.resolve(__dirname, 'dist'),
//         filename: 'index.js',
//     },

//     // для настройки и загрузки loader-ов
//     module: {
//         rules: [
//             {
//                 // Тест - это легулярное выражение в котором мы описываем файлы какого расширение лоадером мы будем обрабатывать
//                 test: /\.[tj]sx?$/,
//                 use: ['ts-loader']
//             }
//         ],
//     },

//     plugins: [
//         new HtmlWebpackPlugin({
//             // webpack относительный или абсолютный путь к шаблону. По умолчанию он будет использоваться src/index.ejs, если он существует.
//             template: path.resolve(__dirname, 'index.html')
//         })
//     ],

//     devServer: {
//         port: 3000,
//         open: true,
//         hot: IS_DEV,
//     },

//     // Dev Tool отвечает за то формируются ли SourceMap у нас в приложении, 
//     // SourceMAp это специальные файлы которые позволяют размать ваше минифицированные и транспирированные js код
//     // который находится в одном большом файле, на все файлы котороые находятся в вашем приложении будь то js или ts,
//     //  это очень сильно помогает development-те, чтобы понять где произошла ошибкавам будет выдоваться
//     // сообщения исходя из ваших src файлов имено благодоря SourceMap-ам, 
//     // а не указывать на сбилженый большой дс файл в котором не разберешь что написано
//     devtool: setupDevTool(),
// };