
// Эта функция которая возвращает html, content добавляем в div
//<script src="/static/client.js" type="application/javascript"></script> -> этот скрипт будет загружать клиенский бандле
//indexTimplate импортируем в server.js и добавляем его в 
// app.get('/', (req, res) => {
//     res.send(
//        indexTimplate(ReactDOM.renderToString(Header())),
//     );
// });
export const indexTimplate = (content) => `
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <script src="/static/client.js" type="application/javascript"></script>  
    </head>
    <body>
        <div id="react-root">${content}</div>
    </body>
    </html>
`