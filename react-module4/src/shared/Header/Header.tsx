import React from 'react';
import styles from './header.less';
import { SearchBlock } from './SearchBlock/SearchBlock';
import { SortBlocke } from './SortBlocke/SortBlocke';
import { ThreadTitle } from './ThreadTitle/ThreadTitle';

export function Header() {
  return (
    <header className={styles.header}>
      <SearchBlock />
      <ThreadTitle />
      <SortBlocke />
    </header>
  );
}
