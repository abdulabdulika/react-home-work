import React from 'react';
import { CommentsMenu } from './CommentsMenu';
import styles from './controlsmenu.less';
import { CounterMenu } from './CounterMenu';
import { SocialLink } from './SocialLink';

export function ControlsMenu() {
  return (
    <div className={styles.controlsBlock}>
      <CounterMenu />
      <CommentsMenu />
      <SocialLink />
    </div>
  );
}
