import React from 'react';
import styles from './iconcontent.less';

export function IconContent() {
  return (
    <div className={styles.blockIcon}>
      <img src="https://i.sunhome.ru/journal/92/mlechnii-put.orig.jpg" alt="" className={styles.icon}/>
    </div>
  );
}
