import React from 'react';
import styles from './user.less';
import { ButtonDrop } from './ButtonDrop';
import { UserInfo } from './UserInfo';

export function User() {
  return (
    <div className={styles.user}>
      <UserInfo />
      <ButtonDrop />
    </div>
  );
}
