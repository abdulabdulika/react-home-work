import React from 'react';
import styles from './userinfo.less';

export function UserInfo() {
  return (
    <div className={styles.userPosition}>
      <div className={styles.userAvatar}></div>
      <p className={styles.nameUser}>
        Дмитрий Грашин
        <span className={styles.indentSpan}>4 часа назад</span>
      </p>
    </div>
  );
}
