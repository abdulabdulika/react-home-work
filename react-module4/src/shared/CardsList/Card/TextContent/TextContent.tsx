import React from 'react';
import styles from './textcontent.less';
import { User } from './User';
import { TextUser } from './TextUser';


export function TextContent() {
  return (
    <div className={styles.textContent}>
      <User />
      <TextUser />
    </div>
  );
}
