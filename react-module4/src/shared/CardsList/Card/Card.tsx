import React from 'react';
import styles from './card.less';
import { ControlsMenu } from './ControlsMenu';
import { IconContent } from './IconContent';
import { TextContent } from './TextContent';

export function Card() {
  return (
    <li className={styles.card}>
      <TextContent />
      <IconContent />
      <ControlsMenu />
    </li>
  );
}
